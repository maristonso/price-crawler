const puppeteer = require('puppeteer');

const SimpleNodeLogger = require('simple-node-logger'),
    opts = {
        logFilePath:'maaamet-crawler.log',
        timestampFormat:'DD.MM.YYYY HH:mm:ss.SSS'
    },
log = SimpleNodeLogger.createSimpleLogger( opts );

const url = 'http://www.maaamet.ee/kinnisvara/htraru/Start.aspx';
let countyList; 
tallinnDistricts = [
    'Viimsi vald',
    'Haabersti linnaosa',
    'Kesklinna linnaosa',
    'Kristiine linnaosa',
    'Lasnamäe linnaosa',
    'Mustamäe linnaosa',
    'Nõmme linnaosa',
    'Pirita linnaosa',
    'Põhja-Tallinna linnaosa'
];
const maxDelay = 5000; 


//------------------------- Minor assistant functions-----------------------------------

randomDelay = () => Math.floor((Math.random() * maxDelay) + 1);
//randomDelay = () => 0; //no delays, uncomment to speed up process of filling

function detectCountyIndex(county){
    let cId;
    for(let i = 1; i<countyList.length; i++){
        if(countyList[i].includes(county)) {cId = i; break;}
    }
    return cId;
}


//====================== Filling Forms ==========================================

async function waitForString(page, reg) {
    let htmlContent;
    await page.waitFor(randomDelay());
    await page.waitForFunction('document.querySelector("body")!==null');
    do {
        await page.waitFor(10);
        htmlContent = await page.content(); 
    }
    while (!reg.test(htmlContent));
};

async function fillFormsShort(page, keyPattern){
    await page.goto(url, {waitUntil: 'networkidle2'});
    await page.click('nav > .right > #RBLLang > label:nth-child(4) > img');
    await waitForString(page, keyPattern);
    return page;
}


async function fillForms(page, cId, cityDistricts){
    const county = countyList[cId];

    try{
        console.log(`Fetching data of ${county}...`)
        await page.goto(url, {waitUntil: 'networkidle2'});
        await page.waitFor(randomDelay());

        //------------------------ select English
        
        await page.click('nav > .right > #RBLLang > label:nth-child(4) > img');
        

        // ---------------------- select price statistics
        
        
        await waitForString(page, /DDTrykis/);
        await page.select('#DDTrykis', 'G'); //1st menu:

        // ---------------------- select transactions of residental appartments
        
        await waitForString(page, /T13/); 
        await page.select('#LBTrykis', 'T13'); //2nd menu
        await waitForString(page, /<option selected="selected" value="T13">/);

        // --------------------------- County

        if(!cityDistricts){              // city districts not required
            await waitForString(page, /id="asukoht"/);
            await page.click(`#chkAsukoht`);            // select 'Detailed search for several regions'
            await page.waitForSelector(`#chkAsukoht`);  //nothing actually works here as expected
            await page.waitFor(randomDelay()); //keep it 
        }   

        await waitForString(page, /id="DDMaakond"/);
        await page.click(`#UpdatePanel1 > section:nth-child(8) > div > button`);//Button opens menu. It HAS failed
        await page.click(`#UpdatePanel1 > section:nth-child(8) > div > ul > li:nth-child(${cId})`); //select county
        if(cityDistricts) {
            await page.click(`#UpdatePanel1 > section:nth-child(8) > div > button`);//Button closes menu:
        }
        let reg = new RegExp(`<option selected="selected"[^>]+>${county}`);
        await waitForString(page, reg);
        
        // ------------------------- Municipality/city districts
        if(cityDistricts) {
            await waitForString(page, /id="DDOmavalitsus"/);
            let html = await page.content();
            const municipalityList = populateList(html, 'Municipality');

            await page.click(`#UpdatePanel1 > section:nth-child(9) > div > button`); // Button opens menu
            for(let i=1; i<municipalityList.length; i++){
                await page.click(`#UpdatePanel1 > section:nth-child(9) > div > ul > li:nth-child(${i})`); //select municipality
            };
            await page.click(`#UpdatePanel1 > section:nth-child(9) > div > button`); //Button closes menu
            let lastMunicipality = municipalityList[municipalityList.length-1];
            reg = new RegExp(`<option selected="selected"[^>]+>${lastMunicipality}`);
            await waitForString(page, reg);  //wait until the last municipality is checked
        }


        // ----------------------------- select time period -----------------------------------------
        await page.click('#RBLAeg > label:nth-child(4)');           // selct time period
        console.log('Forms filled...');
        await waitForString(page, /<input id="RBLAeg_1" type="radio" name="RBLAeg" value="1" checked="checked">/);

        // ----------------------------- Press 'write report' -----------------------------------------

        await page.click('#btnTryki');                             //Button 'write raport'
        await page.waitForNavigation({waitUntil: 'networkidle2'});// NB! Fails, if data not avalible, e.g. c=11, m=1
        await page.waitFor(3*randomDelay());
        log.info(`Price statistics for ${county} successfully fetched`);
        return page;
    }
    catch(err) {
        log.error('Filling forms failed. ', err);
    }
}

//============================== Extracting data from html


function extractTimePeriod(htmlPage){
    const start = htmlPage.indexOf('time period');
    const end = htmlPage.indexOf('<', start);
    let timePeriod = htmlPage.substring(start+12, end);
    timePeriod = timePeriod.split('and').join('-');
    return timePeriod;
}

 function htmlTableToArray(html) { //input is string '<tbody>...............</tbody>'
    let arr = html.split('</tr><tr>'); //separate rows
    arr = arr.filter(row => !row.includes('</th>')); //drop header rows;
    let dataMatrix = [];
    let reg = new RegExp('<td[^>]*>', '');
    arr.forEach( rowString =>
        dataMatrix.push(rowString.split('</td>').join('').split(reg)) //removes </td>, separetes cells and removes <td...>
    );
    return dataMatrix;
}

function reformatData(dataMatrix, cId){//transforms land board data array into new form, more suitable for further exploitation
    const county = countyList[cId];
    let municipality;
    newMatrix = dataMatrix.filter(row => !row[2].includes('ALL TOTAL'));        //remove 'ALL TOTAL' row
    if(county === 'Hiiu maakond' && newMatrix[0].length === 12) {              //dealing with special case, there must be 13 columns
        newMatrix.forEach(row => {
            row[0] = 'Hiiumaa vald';
            row.unshift("");
        });
    }
    newMatrix.forEach(row => {
        if(row[1]!==''){ //if the municipality name is given in row then save it
            municipality = row[1];
        }
        row[0] = county;                        //add county name
        row[1] = municipality;                  //add municipality name
        row.splice(2,0,'');                     //insert a new empty column for area range
        if(row[3]==='TOTAL') {
            row[3] = '10-249.99';
        }
        [row[2], row[3]] = row[3].split('-');   //split area interval column into 2 columns
        row[3] = row[3].replace(',','.');       //this column has different decimal separator, replace it
        [row[4], row[5]] = [row[5], row[4]];    //swap columns 4 and 5
        row.splice(6, 1);                       //removes column 6, containing total sum of transactions
        for(j = 2; j<row.length; j++){
            row[j] = Number( row[j].split(',').join('') ); 
                                                //if not a number then gives 'NaN' and in json there will be 'null'
            if(row[j] % 1 !== 0){
                row[j] = row[j].toFixed(2);
            }
        }
    });
    return(newMatrix)
}

function deleteExcessiveData(dataMatrix, region) {
    if(region === 'Tallinn'){
        newMatrix = dataMatrix.filter(row => tallinnDistricts.includes(row[1]));  //keep only Tallinn districts data
    } else {
        newMatrix = dataMatrix;
    }
    return newMatrix;
}

function extractData(htmlPage, cId){ //htmlPage is string, corresponding to html page; c - county Id
    const start = htmlPage.indexOf('<tbody>');
    const end = htmlPage.indexOf('</tbody>');
    let tableBodyString = htmlPage.substring(start, end+8);
    tableBodyString = tableBodyString.split('&nbsp;').join(''); // remove spaces
    let dataMatrix = htmlTableToArray(tableBodyString);
    dataMatrix = reformatData(dataMatrix, cId);
    let timePeriod = extractTimePeriod(htmlPage);
    dataMatrix.forEach(row => {row = row.push(timePeriod)});
    return dataMatrix;
}

function populateList(htmlContent, keyWord){
    const menuStart = htmlContent.indexOf(keyWord);
    const start = htmlContent.indexOf('<option', menuStart);
    const end = htmlContent.indexOf('</select>', menuStart);
    str = htmlContent.substring(start, end);
    str = str.split("\n").join("");
    str = str.split("\t").join("");
    str = str.split("</option>").join("");
    let reg = new RegExp('<option[^>]*>', '');
    let list = str.split(reg);
    console.log(`${keyWord} list populated`);
    return list;
}


//================================ Main function ========================================

async function fetchPriceStatistics(region, cityDistricts = true) {
    log.info('Crawler started (slow mode, wait at least 20 seconds to see something)');
    const browser = await puppeteer.launch({
        defaultViewport: {width: 1100, height: 1080},
        headless: true,// set to true, if you want hide chromium browser
        devtools: true,
        slowMo: 0,//250 to slow down
        args: [
            '--window-size=1920,1080',
            ]
    });
    let page = await browser.newPage();
    page = await fillFormsShort(page, /id="DDMaakond"/);        //initialize countyList
    let html = await page.content();
    countyList = populateList(html, 'County');                      
    dataMatrixBig = [];
    
    // ================= detecting county and municipality indices =====================

    let cIds = [];                          //county indices - array
    let region1;                            //standardized region description

    if(Array.isArray(region)) { region1 = region; } else {region1 = [region]; } //single string instead of array
    for(let i = 0; i<region1.length; i++){
        if(region1[i] == 'Estonia'){
            for(let i = 1; i<countyList.length; i++){
                cIds.push(i);
            }
        } else if (region1[i] == 'Tallinn') {
            let cId = detectCountyIndex('Harju');
            cIds.push(cId);
        } else {
            let cId = detectCountyIndex(region1[i]);
            cIds.push(cId);
        }
    }
    //========================= fetching data =========================================
    
    for(let i=0; i<cIds.length; i++){
        let dataMatrix = await fillForms(page, cIds[i], cityDistricts)
             .then(page => page.content())
             .then(html => extractData(html, cIds[i]))
             .then(dataMatrix => deleteExcessiveData(dataMatrix, region1[0]))//important for Tallinn
             .catch(err => console.log('fetchPriceStatistics failed'+err));
         dataMatrixBig = dataMatrixBig.concat(dataMatrix);
    }; 
    await browser.close();
    return dataMatrixBig;
};


exports.fetchPriceStatistics = fetchPriceStatistics;









