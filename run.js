
const cr = require('./crawler')
const output = require('./writing-output');

//********************* Calling the main function *******************************


//fetchPriceStatistics(['Hiiu', 'Saare']) fetches data for particular counties
//fetchPriceStatistics('Estonia') to fetches data for all counties
//fetchPriceStatistics('Tallinn') fetches data for Tallinn districts
//fetchPriceStatistics(region, false) excludes districts of larger citys. Simpler and faster but gives less data

cr.fetchPriceStatistics('Estonia')
.then(dataMatrix => {
        output.writeHTMLFile(dataMatrix);
        output.writeJSONFile(dataMatrix);
        output.writeCSVFile(dataMatrix);
})//.catch(err => console.log('Crawler failed'+err))

