const fs = require('fs');
const SimpleNodeLogger = require('simple-node-logger'),
    opts = {
        logFilePath:'maaamet-crawler.log',
        timestampFormat:'DD.MM.YYYY HH:mm:ss.SSS'
    },
log = SimpleNodeLogger.createSimpleLogger( opts );

//==================== HTML ==============================================

const htmlFirstPart =`<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Price Statistics</title>
    <link rel = "stylesheet" href = "priceStatistics.css">
</head>
<body>
<h3>Transactions with residential apartments</h3>`

const htmlLastPart = `
</body>
<script src="priceStatistics.js"></script>
</html>`;

const htmlTableHead = `<thead>
<tr class="header1">
    <th rowspan = "2" class="col0">County</th>
    <th rowspan = "2" class="col1">Municipality</th>
    <th colspan = "3">Area (m^2)</th>
    <th rowspan = "2" class="col5">Number of Transactions</th>
    <th colspan = "2">Price (€)</th>
    <th colspan = "5">Price per unit(€/m^2)</th>
    <th rowspan = "2" class="col13">Time period (mm/dd/yyyy)</th>
</tr>
<tr class="header2">
    <th class="col2">From</th>
	<th class="col3">To</th>
	<th class="col4">Average</th>
    
    <th class="col6">Min</th>
    <th class="col7">Max</th>

    <th class="col8">Min</th>
    <th class="col9">Max</th>
    <th class="col10">Median</th>
    <th class="col11">Average</th>
    <th class="col12">Standard Deviation</th>
    
</tr>
</thead>`

function getHTMLTable(dataMatrix) {
    let result = ['<tbody>'];
    for(let row of dataMatrix) {
        result.push('<tr>');
        for(let j = 0; j<row.length; j++){
            result.push(`<td class="col${j}">${row[j]}</td>`);
        }
        result.push('</tr>');
    }
    result.push('</tbody>');
    return result.join('\n');
  }

function writeHTMLFile(dataMatrix){
    let htmlTableBody = getHTMLTable(dataMatrix);
    let htmlNew = htmlFirstPart + `<table>`+ htmlTableHead + htmlTableBody +`</table>` + htmlLastPart;
    fs.writeFile('priceStatistics.html', htmlNew, err => {
        if (err) {
            log.info('Error writing HTML data file', err)
        } else {
            log.info('Data successfully written to HTML file')
        }
    })
}

// ========================= JSON ======================================

function fromMatrixToObjects(dataMatrix) {
    let objArray = [];
    for(i=0; i<dataMatrix.length; i++){
        let obj = new Object();
        obj.county = dataMatrix[i][0];
        obj.municipality = dataMatrix[i][1];
        obj.minArea = dataMatrix[i][2];
        obj.maxArea = dataMatrix[i][3];
        obj.averageArea = dataMatrix[i][4];
        obj.numberOfTransactions = dataMatrix[i][5]; 
        obj.minPrice = dataMatrix[i][6]; 
        obj.maxPrice = dataMatrix[i][7];
        obj.minPricePerSquareMeter = dataMatrix[i][8];
        obj.maxPricePerSquareMeter = dataMatrix[i][9];
        obj.medianPricePerSquareMeter = dataMatrix[i][10];
        obj.averagePricePerSquareMeter = dataMatrix[i][11];
        obj.standardDeviationPricePerSquareMeter = dataMatrix[i][12];
        obj.timePeriod = dataMatrix[i][13];
        objArray.push(obj);
    }            
    return objArray;
}

function writeJSONFile(dataMatrix){
    let objArray = fromMatrixToObjects(dataMatrix);
    let jsonData = JSON.stringify(objArray);
    fs.writeFile('priceStatistics.json', jsonData, err => {
        if (err) {
            log.info('Error writing JSON data file', err)
        } else {
            log.info('Data successfully written to JSON file')
        }
    })
}

// ==================== CSV ==========================================

const attributes = [
    'County',
    'Municipality',
    'MinArea', 
    'MaxArea', 
    'AverageArea', 
    'NumberOfTransactions', 
    'MinPrice', 
    'MaxPrice', 
    'MinPricePerSquareMeter', 
    'MaxPricePerSquareMeter', 
    'MedianPricePerSquareMeter',
    'AveragePricePerSquareMeter',
    'StandardDeviationPricePerSquareMeter',
    'TimePeriod'
]

function writeCSVFile(dataMatrix){
    let csvString = attributes + '\n';
    for(let i = 0; i<dataMatrix.length; i++){
        csvString = csvString + dataMatrix[i]+'\n';
    }
    fs.writeFile('priceStatistics.csv', csvString, err => {
        if (err) {
            log.info('Error writing CSV data file', err)
        } else {
            log.info('Data successfully written to CSV file')
        }
    })
}

exports.writeHTMLFile = writeHTMLFile;
exports.writeJSONFile = writeJSONFile;
exports.writeCSVFile = writeCSVFile;