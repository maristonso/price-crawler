## Installing

1. Install npm and node.js

2. Install puppeteer package https://github.com/puppeteer/puppeteer. I used command 

	`npm i puppeteer`

	When installed, puppeteer downloads a version of Chromium browser, which it drives.

3. Copy crawler files into some directory, where node is available.

## Running
Run crawler and fetch data by typing 

	node run.js

After successful crawling priceStatistics.csv, priceStatistics.html and priceStatistics.json data files should appaer in the crawler catalog.

## Interpreting results

For each municipality there are usually several data rows; for instance

|County			|Municipality    |Area from |Area to  |Average area |# of transactions|Min price       |Max price |Min unit price|.......|
|---------------|----------------|---------:|--------:|------------:|:---------------:|---------------:|----------:|--------------|-------|
|Harju maakond	|Pirita linnaosa |    	  30| 	 40.99|     	 NaN|                1|             NaN|        NaN|           NaN|.......|
|Harju maakond	|Pirita linnaosa |    	  55| 	 69.99|          NaN|                1|             NaN|        NaN|           NaN|.......|
|Harju maakond	|Pirita linnaosa |    	  70| 	249.99|  	113.90	|                7|          122000|      320000|      1740.37|.......|
|Harju maakond	|Pirita linnaosa |    	  10| 	249.99|  	99.40	|                9|           93000|      320000|      1740.37|.......|

'Area from' and 'Area to' specify the interval of area of apartments.

`Nan`-symbols appearing in table are normal in case of smaller municipalities. The land board does not provide data, if 5 or less transactions have occured and sometimes data is hidden even if there has been more transactions. Sometimes it may also happen, that there is no row for particular area interval at all. In above table interval 41-54.99m^2 is missing - I guess then there has been 0 transactions.

The **last row** contains average prices for all areas (10-249.99m^2). If no numbers are provided for the paricular area interval (row is missing or filled with Nan-s), then the last row can be used as an approximation.

## Main function

`fetchPriceStatistics(region, cityDistricts)`

Opens Estonian land board website http://www.maaamet.ee/kinnisvara/htraru/Start.aspx and fetches price statistics for the given region. The function returns data as 2D array.

Possible values of region:

`'Tallinn'` - fetches price data for Tallinn districts.

`'Estonia'` - fetches data for all counties.

`[county1, county2,...]` - fetches data for the given counties. Possible counties: 'Harju', 'Hiiu', 'Ida-Viru', 'Jõgeva', 'Järva', 'Lääne', 'Lääne-Viru', 'Põlva', 'Pärnu', 'Rapla', 'Saare', 'Tartu', 'Valga', 'Viljandi' and 'Võru'.

Region can be changed in file run.js

`cityDistricts` - true or false, default value is true.

`true` includes districts of larger cities. It provides more data, but form-filling process is more complicated as it checks all municipalities one-by-one. See also `fillForms` function.

`false` checks 'Detailed search for several regions', then selects county and checks 'all municipalities'.


## Assistant functions

`fillForms(page, countyId, cityDistricts)`

Fills forms on land board website.

page - puppeteer webpage object

countyId - an integer, corresponding to the county id number on land board website.

cityDistricts - true or false; indicates alternative crawling scenarios

If cityDistricts is true, the forms are filled as follows:  
Language -> English  
Type of publication -> Price statistics  
Report -> Transactions with residental apartments  
Cut percentage -> 0 (remains unchanged)  
Location -> nothing is checked  
County -> selects countyId  
Municipality, city district -> checks all one-by-one  
Time period -> checks 'last month'  


If cityDistricts is false, the forms are filled as follows:  
Language -> English  
Type of publication -> Price statistics  
Report -> Transactions with residental apartments  
Cut percentage -> 0 (remains unchanged)  
Location -> checks 'Detailed search for several regions'  
County -> selects countyId  
'All municipalities' -> checked  
Time period -> checks 'last month'  

Returns puppeteer page object - the one containing price data.

`extractData(page, countyId)`

page - puppeteer webpage object, the page with price data returned by land board website.

Returns 2D array, containing the price data.

`writeCSVFile(dataMatrix)`

Converts price statistics data into csv format and writes it to the file priceStatistics.csv.

dataMatrix - 2D array with specific structure

`writeHTMLFile(dataMatrix)`

Converts price statistics data into html table format and writes to the file priceStatistics.html.

dataMatrix - 2D array with specific structure

`writeJSONFile(dataMatrix)`

Converts price statistics data into json format and writes to the file priceStatistics.json.

dataMatrix - 2D array with specific structure

## Some settings

To make Chromium browser unvisible, go to file `crawler.js`, find function `fetchPriceStatistics()`, find command `puppeteer.launch(...)` and set `headless: true`.

To speed up process of filling forms, got to file `crawler.js` and uncomment `randomDelay = () => 0`.
